//
//  Manga_ReaderApp.swift
//  Manga Reader
//
//  Created by Kazoryah LeGrand on 29/12/2020.
//

import SwiftUI

@main
struct Manga_ReaderApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
